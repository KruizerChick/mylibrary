#!/usr/bin/env python3.8
"""Django's command-line utility for administrative tasks."""
import os
import sys

# Use .env file for environmental and universal settings
from dotenv import load_dotenv
from pathlib import Path  # python3 only
env_path = Path('.') / '.env'

load_dotenv(dotenv_path=env_path)

if __name__ == '__main__':
    # os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.prod')
    if os.getenv('DJANGO_ENVIRONMENT_NAME') is None:
        # If no env variable of DJANGO_ENVIRONMENT_NAME, assume development
        print("Env variable not found. Using config.settings.local")
        os.environ.setdefault(
            "DJANGO_SETTINGS_MODULE", "config.settings.local")

    else:
        # Use Environment setting to determine settings file to load
        print("Django Env is:", os.getenv('DJANGO_ENVIRONMENT_NAME'))
        env_options = {
            'Development': 'config.settings.local',
            'PA_Prod': 'config.settings.prod',
        }
        current_env = env_options[os.getenv('DJANGO_ENVIRONMENT_NAME')]

        os.environ.setdefault(
            "DJANGO_SETTINGS_MODULE", current_env)

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    execute_from_command_line(sys.argv)
