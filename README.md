# myLibrary

**myLibrary** is a Django web site application to track, rate and review books that you've read or want to read.

## Features
- Rating system allows rating and comments for nine different categories, then averages out those categories into an overall rating for the book.
- Allows books to be marked with a status of Want to Read, Did Not Finish, or Read.


## Built With

* [Python 3.8](https://docs.python.org/3/whatsnew/3.8.html) - The base language used
* [Django 3](https://docs.djangoproject.com/en/3.0/) - The web framework
* [python-dotenv](https://github.com/theskumar/python-dotenv) - Used to manage environmental variables


## License



## Acknowledgments

* Thanks to Seryniti for the [article](https://myseryniti.wordpress.com/2012/08/01/top-10-things-that-help-me-rate-a-book/) that gave me my rating criteria.

