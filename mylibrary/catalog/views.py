from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import Book, Author, Genre


def index(request):
    """ View function for home page of site. """

    # Generate counts of some of the main objects
    num_books = Book.objects.count()
    num_authors = Author.objects.count()
    num_genres = Genre.objects.count()

    # Number of visits to this view, as counted in the session variable.
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    context = {
        'num_books': num_books,
        'num_authors': num_authors,
        'num_genres': num_genres,
        'num_visits': num_visits,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'catalog/index.html', context=context)


# Book views
class BookListView(ListView):
    """ View for List of Books """
    model = Book
    paginate_by = 20


class BookDetailView(DetailView):
    model = Book


class BookCreate(PermissionRequiredMixin, CreateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_add_books'


class BookUpdate(PermissionRequiredMixin, UpdateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_change_books'


class BookDelete(PermissionRequiredMixin, DeleteView):
    model = Book
    success_url = reverse_lazy('books')
    permission_required = 'catalog.can_delete_books'


# Author views
class AuthorListView(ListView):
    """Generic class-based list view for a list of authors."""
    model = Author
    paginate_by = 20


class AuthorDetailView(DetailView):
    """Generic class-based detail view for an author."""
    model = Author


class AuthorCreate(CreateView):
    model = Author
    fields = '__all__'
    # initial = {'date_of_death': '05/01/2018'}
    permission_required = 'catalog.can_add_authors'


class AuthorUpdate(UpdateView):
    model = Author
    fields = [
        'first_name', 'last_name', 'pseudonyms', 'bio',
        'date_of_birth', 'date_of_death'
    ]
    permission_required = 'catalog.can_change_authors'


class AuthorDelete(DeleteView):
    model = Author
    success_url = reverse_lazy('authors')
    permission_required = 'catalog.can_delete_authors'


# Genre views
def show_genres(request):
    """ View function for a list of Genres """
    return render(
        request, "catalog/genres.html", {'genres': Genre.objects.all()})


class GenreDetail(DetailView):
    """ Genre detail view """
    model = Genre
    template_name = 'catalog/genre_detail.html'


class GenreCreate(PermissionRequiredMixin, CreateView):
    model = Genre
    fields = '__all__'
    permission_required = 'catalog.can_add_genres'


class GenreUpdate(PermissionRequiredMixin, UpdateView):
    model = Genre
    fields = '__all__'
    permission_required = 'catalog.can_change_genres'


class GenreDelete(PermissionRequiredMixin, DeleteView):
    model = Genre
    success_url = reverse_lazy('genres')
    permission_required = 'catalog.can_delete_genres'
