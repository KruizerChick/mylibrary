# Generated by Django 3.0.3 on 2020-03-03 17:25

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100, verbose_name='first name')),
                ('middle_name', models.CharField(blank=True, max_length=50, verbose_name='middle name')),
                ('last_name', models.CharField(max_length=100, verbose_name='last name')),
                ('date_of_birth', models.DateField(blank=True, null=True, verbose_name='born')),
                ('date_of_death', models.DateField(blank=True, null=True, verbose_name='died')),
            ],
            options={
                'ordering': ['last_name', 'first_name'],
            },
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('summary', models.TextField(help_text='Enter a brief description of the book (max char=1000).', max_length=1000, verbose_name='summary')),
                ('isbn', models.CharField(blank=True, help_text='13 Character ISBN number', max_length=13, verbose_name='ISBN')),
                ('status', models.CharField(choices=[('WTR', 'Want to Read'), ('CR', 'Currently Reading'), ('DNF', 'Did Not Finish'), ('R', 'Read')], default='WTR', max_length=3, verbose_name='status')),
                ('overall_rating', models.PositiveSmallIntegerField(default=0, verbose_name='overall rating')),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now, verbose_name='added')),
                ('date_read', models.DateField(blank=True, help_text='Enter the date when read.', null=True, verbose_name='date read')),
                ('date_published', models.DateField(blank=True, help_text='Enter the date of publication.', null=True, verbose_name='date published')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Author')),
            ],
            options={
                'ordering': ['-date_added'],
            },
        ),
        migrations.CreateModel(
            name='RatingType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Type of rating.', max_length=50, verbose_name='type')),
                ('slug', models.SlugField(max_length=60, unique=True, verbose_name='slug')),
                ('description', models.TextField(help_text='Description of the rating type.', verbose_name='description')),
            ],
            options={
                'verbose_name': 'Rating Type',
                'verbose_name_plural': 'Rating Types',
            },
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rating', models.PositiveSmallIntegerField(choices=[(None, 'None'), (1, '1 Star'), (2, '2 Stars'), (3, '3 Stars'), (4, '4 Stars'), (5, '5 Stars')], help_text='Rate where 1 is low and 5 is high', verbose_name='rating')),
                ('comments', models.TextField(blank=True, help_text='Comments supporting this rating.', max_length=500, verbose_name='comments')),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ratings', to='catalog.Book')),
                ('rating_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ratings', to='catalog.RatingType')),
            ],
            options={
                'verbose_name': 'Rating',
                'verbose_name_plural': 'Ratings',
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Enter a book genre (e.g. Science Fiction)', max_length=200, unique=True, verbose_name='name')),
                ('slug', models.SlugField(help_text="Used to build the genre's URL.", max_length=255, unique=True, verbose_name='slug')),
                ('definition', models.TextField(blank=True, help_text='Short description of the genre.', max_length=400, verbose_name='definition')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, help_text='If this genre is a sub-genre, what does it belong to?', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='catalog.Genre', verbose_name='parent genre')),
            ],
            options={
                'ordering': ['-name'],
            },
        ),
        migrations.AddField(
            model_name='book',
            name='genre',
            field=mptt.fields.TreeManyToManyField(blank=True, help_text='Select a genre for this book.', related_name='books', to='catalog.Genre'),
        ),
        migrations.AddIndex(
            model_name='book',
            index=models.Index(fields=['author', 'title'], name='catalog_boo_author__93642e_idx'),
        ),
    ]
