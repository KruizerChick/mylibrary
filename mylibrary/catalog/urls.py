""" URLs for Catalog app """
from django.urls import path

from . import views

app_name = 'catalog'

urlpatterns = [
    path('', views.index, name='index'),

    # URLs for Books
    path('books/', views.BookListView.as_view(), name='books'),
    path('book/<int:pk>', views.BookDetailView.as_view(), name='book_detail'),
    path('book/create/', views.BookCreate.as_view(), name='book_create'),
    path('book/<int:pk>/update/', views.BookUpdate.as_view(),
         name='book_update'),
    path('book/<int:pk>/delete/', views.BookDelete.as_view(),
         name='book_delete'),

    # URLs for Authors
    path('authors/', views.AuthorListView.as_view(), name='authors'),
    path('author/<int:pk>',
         views.AuthorDetailView.as_view(), name='author_detail'),
    path('author/create/', views.AuthorCreate.as_view(), name='author_create'),
    path('author/<int:pk>/update/', views.AuthorUpdate.as_view(),
         name='author_update'),
    path('author/<int:pk>/delete/', views.AuthorDelete.as_view(),
         name='author_delete'),

    # URLs for Genres
    path('genres/', views.show_genres, name='genres'),
    path('genres/<slug:slug>', views.GenreDetail.as_view(),
         name='genre_detail'),
    path('genre/create/', views.GenreCreate.as_view(), name='genre_create'),
    path('genre/<int:pk>/update/', views.GenreUpdate.as_view(),
         name='genre_update'),
    path('genre/<int:pk>/delete/', views.GenreDelete.as_view(),
         name='genre_delete'),
]
