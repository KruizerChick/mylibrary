from django.db import models
from django.urls import reverse
from django.utils import timezone
# from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from mptt.managers import TreeManager
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField


class Genre(MPTTModel):
    """ Model representing a book genre. """
    name = models.CharField(
        _('name'), max_length=200, unique=True,
        help_text=_('Enter a book genre (e.g. Science Fiction)'))
    slug = models.SlugField(
        _('slug'),
        # unique=True,
        max_length=255,
        help_text=_("Used to build the genre's URL."))
    definition = models.TextField(
        _('definition'), max_length=400, blank=True,
        help_text=_('Short description of the genre.'))
    parent = TreeForeignKey(
        'self', on_delete=models.CASCADE, null=True, blank=True,
        related_name='children', verbose_name=_('parent genre'),
        help_text=_('If this genre is a sub-genre, what does it belong to?')
        )

    objects = TreeManager()

    @property
    def tree_path(self):
        """
        Returns genre's tree path by concatening the slug of his ancestors.
        """
        if self.parent_id:
            return '/'.join(
                [ancestor.slug for ancestor in self.get_ancestors()] +
                [self.slug])
        return self.slug

    def get_absolute_url(self):
        """
        Builds and returns the genre's URL based on his tree path.
        """
        return reverse('catalog:genre_detail', args=(self.tree_path,))

    class MPTTMeta:
        """ Genre MPTT meta information. """
        order_insertion_by = ['name']

    class Meta:
        """ Genre meta information. """
        ordering = ['-name']

    def __str__(self):
        """String for representing the Genre object (in Admin site etc.)."""
        return self.name


class Author(models.Model):
    """Model representing an author."""
    first_name = models.CharField(_('first name'), max_length=100)
    middle_name = models.CharField(
        _('middle name'), max_length=50, blank=True)
    last_name = models.CharField(_('last name'), max_length=100)
    date_of_birth = models.DateField(_('born'), null=True, blank=True)
    date_of_death = models.DateField(_('died'), null=True, blank=True)
    bio = models.TextField(_('biography'), blank=True)
    pseudonyms = models.CharField(
        _('pseudonyms'), max_length=200, blank=True)

    class Meta:
        ordering = ['last_name', 'first_name']

    def get_absolute_url(self):
        """Returns the url to access a particular author instance."""
        return reverse('catalog:author_detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Author object."""
        return f'{self.last_name}, {self.first_name}'


class Book(models.Model):
    """ Model representing an instance of a Book. """

    class BookStatus(models.TextChoices):
        """ Choices for Book's status """
        WANT_TO_READ = 'WTR', _('Want to Read')
        CURRENTLY_READING = 'CR', _('Currently Reading')
        DID_NOT_FINISH = 'DNF', _('Did Not Finish')
        READ = 'R', _('Read')

    title = models.CharField(_('title'), max_length=200)
    author = models.ForeignKey(
        Author, null=True,
        on_delete=models.SET_NULL)
    status = models.CharField(
        _('status'), max_length=3,
        choices=BookStatus.choices,
        default=BookStatus.WANT_TO_READ)
    overall_rating = models.PositiveSmallIntegerField(
        _('overall rating'), default=0)
    # Book Details
    summary = models.TextField(
        _('summary'), max_length=1000,
        help_text=_('Enter a brief description of the book (max char=1000).'))
    isbn = models.CharField(
        _('ISBN'), max_length=13, blank=True,
        help_text=_('13 Character ISBN number')
        )
    genre = TreeManyToManyField(
        Genre, blank=True, related_name='books',
        help_text=_('Select a genre for this book.'))
    pages = models.PositiveSmallIntegerField(
        _('pages'), null=True, help_text=_('Number of pages.'))
    # Dates
    date_added = models.DateTimeField(
        _('added'), default=timezone.now)
    date_read = models.DateField(
        _('date read'), null=True, blank=True,
        help_text=_('Enter the date when read.'))
    date_published = models.DateField(
        _('date published'), null=True, blank=True,
        help_text=_('Enter the date of publication.'))

    objects = models.Manager()

    class Meta:
        ordering = ['-date_added', ]
        indexes = [models.Index(fields=['author', 'title']), ]

    def __str__(self):
        """ String for representing the Book object. """
        return self.title

    def get_absolute_url(self):
        """ Returns the url to access a detail record for this book. """
        return reverse("catalog:book_detail", kwargs={"pk": self.pk})

    def display_genre(self):
        """
        Creates a string for the Genre. This is required to
        display genre in Admin.
        """
        return ', '.join([genre.name for genre in self.genre.all()[:3]])

    display_genre.short_description = 'Genre'

    # def save(self, *args, **kwargs):
    #     """ Update fields with calculations upon saving """

        # def calculate_rating(self):
        #     """ Populates overall_rating field with calculated rating """
        #     overall = Rating.objects.filter(book=self).aggregate(
        #         models.Avg('rating'))
        #     return overall
        # self.overall_rating = calculate_rating

        # def get_rating(self):
        #     """ Populates overall_rating field with calculated rating """
        #     overall = Rating.objects.filter(book=self).annotate(
        #         avg=models.Avg('book__ratings'))
        #     return overall
        # self.overall_rating = get_rating

    #    super(Book, self).save(*args, **kwargs)


class RatingType(models.Model):
    """ Model representing the various rating types. """
    name = models.CharField(
        _('type'), max_length=50,
        help_text=_('Type of rating.'))
    slug = models.SlugField(
        _('slug'), max_length=60, unique=True)
    description = models.TextField(
        _('description'),
        help_text=_('Description of the rating type.'))

    class Meta:
        verbose_name = _("Rating Type")
        verbose_name_plural = _("Rating Types")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("RatingType_detail", kwargs={"pk": self.pk})


class Rating(models.Model):
    """ Model representing the actual rating assigned to a book. """

    class RateChoices(models.IntegerChoices):
        """ Choices for the Rating fields """
        LOWEST = 1, _('1 Star')
        LOW = 2, _('2 Stars')
        OK = 3, _('3 Stars')
        HIGH = 4, _('4 Stars')
        HIGHEST = 5, _('5 Stars')

        __empty__ = _('None')

    book = models.ForeignKey(
        Book, related_name='ratings', on_delete=models.CASCADE)
    rating_type = models.ForeignKey(
        RatingType, related_name='ratings', on_delete=models.CASCADE)
    rating = models.PositiveSmallIntegerField(
        _('rating'), choices=RateChoices.choices,
        help_text=_('Rate where 1 is low and 5 is high'))
    comments = models.TextField(
        _('comments'), max_length=500, blank=True,
        help_text=_('Comments supporting this rating.'))

    class Meta:
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")

    def __str__(self):
        return '{0} - {1}'.format(self.book.title, self.rating_type.name)

    def get_absolute_url(self):
        return reverse("Rating_detail", kwargs={"pk": self.pk})
