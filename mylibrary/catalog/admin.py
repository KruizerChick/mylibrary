from django.contrib import admin
from django.urls import NoReverseMatch
from django.utils.html import format_html

from django.utils.translation import ugettext_lazy as _

from mptt.admin import MPTTModelAdmin, TreeRelatedFieldListFilter

from .models import Author, Genre, Book, RatingType, Rating


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = (
        'last_name', 'first_name', 'pseudonyms',
        'date_of_birth', 'date_of_death')
    fields = [
        'first_name', 'middle_name', 'last_name',
        ('date_of_birth', 'date_of_death'),
        'bio', 'pseudonyms'
        ]


class RatingInline(admin.TabularInline):
    model = Rating
    extra = 2
    max_num = 9


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            "fields": (
                ('title', 'author'), ('status', 'date_read'),
                'overall_rating',
                # 'get_rating'
            ),
        }),
        ('Book Details', {
            "fields": (
                'genre', 'summary',
                ('isbn', 'date_published', 'pages'),
            )
        })
    )
    list_display = (
        'title', 'author', 'display_genre',
        'pages', 'status', 'overall_rating',
        )
    list_filter = (
        'status', 'author',
        ('genre', TreeRelatedFieldListFilter),
    )
    search_fields = ('author', 'title', 'summary')
    readonly_fields = ('overall_rating', )
    inlines = [RatingInline]
    save_on_top = True


@admin.register(Genre)
class GenreAdmin(MPTTModelAdmin):
    """ Admin for Genre model """
    fields = ['parent', ('name', 'slug'), 'definition']
    list_display = (
        'name', 'parent', 'slug',
        'get_tree_path',
        'definition'
    )
    prepopulated_fields = {'slug': ('name', )}
    list_filter = ('parent', )

    # def __init__(self, model, admin_site):
    #     self.form.admin_site = admin_site
    #     super(GenreAdmin, self).__init__(model, admin_site)

    def get_tree_path(self, genre):
        """ Return the genre's tree path in HTML. """
        try:
            return format_html(
                '<a href="{}" target="blank">/{}/</a>',
                genre.get_absolute_url(), genre.tree_path)
        except NoReverseMatch:
            return '/%s/' % genre.tree_path
    get_tree_path.short_description = _('tree path')


@admin.register(RatingType)
class RatingTypeAdmin(admin.ModelAdmin):
    """ Admin model for Rating model """
    list_display = ('name', 'slug', 'description')
    prepopulated_fields = {'slug': ('name', )}
