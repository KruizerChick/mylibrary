"""
Django local/development settings for My Library project.

Generated by 'django-admin startproject' using Django 3.0.3.
https://docs.djangoproject.com/en/3.0/ref/settings/#settings

Local Development Configurations:
- Run in Debug mode
- Add Django Debug Toolbar

"""

import os
from .base import *  # noqa


# ADMIN CONFIGURATION
# -----------------------------------------------------------------------------
ENVIRONMENT_NAME = 'Development'
ENVIRONMENT_COLOR = '#00cd00'


# GENERAL
# -----------------------------------------------------------------------------
DEBUG = True
SECRET_KEY = os.getenv("SECRET_KEY")
ALLOWED_HOSTS = ["localhost", "0.0.0.0", "127.0.0.1"]

SITE_ID = 1


# STATIC and MEDIA FILES
# -----------------------------------------------------------------------------
STATICFILES_DIRS = [os.path.join(APPS_DIR, "static")]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]


# DATABASES
# -----------------------------------------------------------------------------
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# CACHES
# -----------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}


# EMAIL
# -----------------------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# django-debug-toolbar
# -----------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites  # noqa
INSTALLED_APPS += ["debug_toolbar"]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware  # noqa
MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config  # noqa
DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips  # noqa
INTERNAL_IPS = ["127.0.0.1", "10.0.2.2"]


# django-extensions
# -----------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration  # noqa
# INSTALLED_APPS += ["django_extensions"]  # noqa F405

