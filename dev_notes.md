# Development Notes

## Future
- Add calculated field that find the average of the ratings.
    - Discard any ratings left blank (if only 5 filled in, divide by 5)
    - Look at aggregate and models.Avg commands
    - See examples on [stack overflow](https://stackoverflow.com/questions/10769027/averaging-input-from-django-admin-form-and-adding-it-to-model-field)
- Use permissions to show or hide action buttons on pages.
- Add tagging capabilities for books.
- Add capability to sort and filter book and author lists.

>Next: Begin Part 10: [Testing](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing)

## Completed
2020-03-04:
- Worked on styling:
    - Added sidebar and top banner image
    - Fixed style for list and detail pages
    - Added custom styling for Admin and various elements (branding, buttons)
- Set up structure for static files
- Configured `gulpfile.js` and installed npm components.

2020-03-03:
- Completed through Part 9: [Working with forms](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Forms)
- Added `bio` and `pseudonyms` fields to Author model.
- Made Genre model an MPTT model where you can have sub-genres. Example: List book as Fiction / Nonfiction then do further subdivision by genre.
- Added slug field and tree path to Genre model.
- Updated Book model to reflect changes to Genre as needed.
- Fixed applications list in `base.py` to include the admindocs app.
- Added `FIXTURES` setting in `base.py` to allow for easy export / import functionality.

2020-03-02:
- Tweaked models / admin views
- Got base templates working for site home page.
- Completed through Part 5: [Create the home page](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Home_page)

2020-03-01:
- Completed through Part 4: [Django admin site](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Admin_site)
- Set up skeletal project for Book Library loosely following Mozilla's Django 
  tutorial: <https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Tutorial_local_library_website>
- Source code: [Github](https://github.com/mdn/django-locallibrary-tutorial)
